<?php
namespace lemon\assets;

use yii\web\AssetBundle;


/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CKEditorAsset extends AssetBundle
{
	public $sourcePath = '@bower/ckeditor';
	//public $sourcePath= '@vendor/lemonvine/yii2-adminlte/dist';

	public $js = [
		'ckeditor.js',
	];

	public $depends = array(
		'yii\bootstrap4\BootstrapAsset'
	);
}
