<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$this->registerAssetBundle(Yii::$app->params['classname_asset']);
$admin_vhost = Yii::$app->params['admin_vhost'];
$admin_name = Yii::$app->params['admin_name'];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title).' - '.$admin_name?></title>
		<link rel="icon" type="image/x-icon" class="js-site-favicon" href="<?= $admin_vhost ?>/favicon.ico">
		<?php $this->head() ?><script src="https://cdn.staticfile.org/angular.js/1.4.6/angular.min.js"></script>
		<?=$this->render('_script.php')?>
	</head>
	<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-sm">
		<input type="hidden" id="chain_menu" value="<?=$this->context->m ?>" />
		<div class="wrapper">
		<?php $this->beginBody() ?>
			<?=$this->render('adminlte-header.php')?>
			<?=$this->render('adminlte-left.php')?>
			<?=$this->render('adminlte-content.php', ['content' => $content])?>
		<?php $this->endBody() ?>
		</div>
	</body>
</html>
<?php $this->endPage() ?>